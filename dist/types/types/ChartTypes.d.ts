export declare type TTooltipTriggers = 'item' | 'axis';
export declare type TTooltipTriggerOn = 'mousemove';
export declare type TChartDataType = 'bar' | 'sankey';
export declare type TDataSetType = 'bar';
