export interface ITitle {
    text?: string;
    subtext?: string;
}
