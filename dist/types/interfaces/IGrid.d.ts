export interface IGrid {
    left?: string;
    right?: string;
    bottom?: string;
    top?: string;
    containLabel?: boolean;
}
