export type TTooltipTriggers = 'item'|'axis';
export type TTooltipTriggerOn = 'mousemove';
export type TChartDataType = 'bar'|'sankey';
export type TDataSetType = 'bar';