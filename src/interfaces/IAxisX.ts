export interface IAxisX {
    data?: string[]
    type?: 'category',
    splitLine?: { show: boolean },
}